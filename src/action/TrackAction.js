import types from './TrackActionType';
import { routerActions } from 'react-router-redux';

export const setCurrentTrack = (track) => {
    return (dispatch) => {
        dispatch({ type: types.SET_CURRENT_TRACK , track });
        dispatch(routerActions.push('/player'));
    };
};

export const routeTo = (path) => {
    return (dispatch) =>{
        dispatch(routerActions.push("/"+path))
    }
};
