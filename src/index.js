import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import routes from "./routes";
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from "react-tap-event-plugin";
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {orange800} from 'material-ui/styles/colors';


injectTapEventPlugin();


const muiTheme = getMuiTheme({
        palette: {
            textColor: orange800,
        },
        appBar: {
            height: 60,
            color:orange800
        },
        });

ReactDOM.render(
    <MuiThemeProvider muiTheme={muiTheme}>
            {routes}
    </MuiThemeProvider>,
    document.getElementById("root")
);

