import React, { Component } from 'react';
import ListToggle from './ListToggle';
import './Item.css';
import PropTypes from 'prop-types';

export default class Item extends Component {
  
  static propTypes = {
      overview: PropTypes.string,
      title:PropTypes.string,
      content: PropTypes.object,
      backdrop: PropTypes.string,
      onTouchTap: PropTypes.func,
      score : PropTypes.number
  }

  onTouchTap=()=>{
      this.props.onTouchTap(this.props.content);
  }

  render() {
    return (
          <div className="Item" 
            onClick={this.onTouchTap}
            style={{backgroundImage: 'url(' + this.props.backdrop + ')'}} >
            <div className="overlay">
              <div className="title">{this.props.title}</div>
              <div className="rating">{this.props.score} / 10</div>
              <div className="plot">{this.props.overview}</div>
              <ListToggle />
            </div>
          </div>
        );
  }
}