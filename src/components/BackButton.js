import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Back from 'material-ui/svg-icons/navigation/arrow-back';
import './BackButton.css';

export default class BackButton extends Component {
  
  static propTypes = {
      back : PropTypes.object
  }

  
  render() {
 
        const backoverlay = {
            back: {
                'width': '48px',
                'cursor': 'pointer',
                'height': '48px'
            },
            container:{
                'position': 'absolute',
                'width': '52px',
                'zIndex': '10',
                'left': '0px',
                'padding': '20px'
            }
        }

    return (
          <div>
               {window.innerWidth > 800 &&
                    <div id="backoverlay" style={backoverlay.container} >
                        <Back style={backoverlay.back} onClick={this.props.back.goBack} ></Back>
                    </div>} 
          </div>
        );
  }
}