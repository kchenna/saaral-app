import React, { Component } from 'react';
import Cast from 'material-ui/svg-icons/hardware/cast';
import PropTypes from 'prop-types';

export default class MovieCard extends Component {

  static propTypes = {
        track: PropTypes.object,
        onTouchTap : PropTypes.func
    };
  

  render() {

    const onItemTouchTap = (e) => {
        e.preventDefault();
        this.props.onTouchTap(this.props.track);
    };

    return (
            <div className="infinite-list-card style_prevu_kit" onClick={onItemTouchTap}>
                <div className="infinite-card-image">
                  <img src={this.props.track.posterImage} width="100%" height="100%" alt={"Poster"}/>
                </div>
                <div className="infinite-card-footer">
                    <div className="infinite-card-title" ><p>{this.props.track.title}</p></div>
                    <div className="infinite-card-cast" >
                        <Cast></Cast>
                    </div>
                </div>
            </div>
    );
  }
}