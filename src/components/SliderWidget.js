import React, { Component } from 'react';
import Item from './Item';
import ItemsCarousel from 'react-items-carousel';
import RightArrow from 'material-ui/svg-icons/hardware/keyboard-arrow-right';
import LeftArrow from 'material-ui/svg-icons/hardware/keyboard-arrow-left';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import PropTypes from 'prop-types';
import './Slider.css';

export default class SliderWidget extends Component {

    constructor(props){
        super(props);
        this.state ={
            mounted : false,
            activeItemIndex: 0,
        };
    }

    static propTypes = {
        data: PropTypes.array,
        onTouchTap: PropTypes.func,
        title: PropTypes.string,
        onViewAll: PropTypes.func,
        path : PropTypes.string
    }

    componentDidMount() {
        this.setState({mounted:true});
    }

    changeActiveItem = (activeItemIndex) => this.setState({ activeItemIndex });

    goTo =() =>{
        this.props.onViewAll(this.props.path);
    }

    getItems = () =>{
        const contents =[];
        const {
            onTouchTap,
            data
        } = this.props;
        if(data) {
            data.forEach(function(content) {
                const score = (content.likeCount / (content.dislikeCount + content.likeCount) ) * 10;
                const rounded = Math.round( score * 10 ) / 10;
                contents.push(
                    <Item key={content.sno} 
                    onTouchTap={onTouchTap}
                    content={content}
                    title={content.title} score={rounded} overview={content.title} backdrop={content.posterImage} />
                );  
            }); 
        }
        return contents;
    }

    render (){

        const {
            activeItemIndex,
            mounted
        } = this.state;
        
        let carouselSettings = {
            numberOfPlaceholderItems : 5,
            numberOfCards : 3,
            slidesToScroll : 3,
            height : 100   
        }

        if ( window.innerWidth > 400 ){
            carouselSettings = {
                numberOfPlaceholderItems : 10,
                numberOfCards : 5,
                slidesToScroll : 5,
                height : 200   
            }
        }

        return (
            <div ref="titlecategory" className="TitleList" data-loaded={mounted}>
                <div className="Title">
                    <div className="TitleContainer" style={{display:'flex',justifyContent:'space-between',marginRight:'20px'}}>
                        <div> <h2>{this.props.title}</h2> </div>
                        { this.props.path &&
                            <div> <h2 style={{cursor:'pointer'}} onClick={this.goTo}>View All </h2></div> }
                    </div>
                    <div>
                        <ItemsCarousel
                            enablePlaceholder
                            numberOfPlaceholderItems={carouselSettings.numberOfPlaceholderItems}
                            minimumPlaceholderTime={1000}
                            placeholderItem={<div style={{ height: carouselSettings.height }}>Loading ...</div>}
                            numberOfCards={carouselSettings.numberOfCards}
                            slidesToScroll={carouselSettings.slidesToScroll}
                            gutter={12}
                            showSlither={true}
                            firstAndLastGutter={true}
                            freeScrolling={false}
                            requestToChangeActive={this.changeActiveItem}
                            activeItemIndex={activeItemIndex}
                            activePosition={'left'}
                            chevronWidth={10}
                            rightChevron={
                                <FloatingActionButton mini={true}>
                                    <RightArrow />
                                </FloatingActionButton>
                            }
                            leftChevron={
                                <FloatingActionButton  mini={true}>
                                    <LeftArrow />
                                </FloatingActionButton>
                            }
                            outsideChevron={false}
                        >
                            {this.getItems()}
                        </ItemsCarousel>
                    </div>
                </div>
            </div>
        );
    }
  
}