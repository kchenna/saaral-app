import React, { Component, PropTypes } from 'react';
import FlatButton from 'material-ui/FlatButton';
import Toggle from 'material-ui/Toggle';
import Paper from 'material-ui/Paper';
import Cast from 'material-ui/svg-icons/hardware/cast';
import Back from 'material-ui/svg-icons/navigation/arrow-back';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import { Player,ControlBar,Shortcut,LoadingSpinner,BigPlayButton   } from 'video-react';
import "../../node_modules/video-react/dist/video-react.css"; 

export default class CustomPlayer extends Component {
  
    static propTypes = {
        router: PropTypes.object,
        url:PropTypes.string,
        player: PropTypes.object,
        track : PropTypes.object
    }

    constructor(props, context) {
        super(props, context);
    }

 componentDidMount() {
    // subscribe state change
    if (this.refs && this.refs.player){
        this.refs.player.subscribeToStateChange(this.handleStateChange.bind(this));
    }
  }

  handleStateChange(state, prevState) {
    // copy player state to this component's state
    this.setState({
      player: state
    });
  }

  render() {

    console.log("this.props.url.indexOf(cdnplay.com) ",this.props.url );

    const goBack = () =>{
        this.props.router.goBack();
    }

    const backoverlay = {
        'position': 'absolute',
        'width': '52px',
        'zIndex' : '10',
        'left':'100px',
        'padding':'20px',
        back:{
            'width':'48px',
            'cursor':'pointer',
            'height':'48px'
        }
    }
    
    const playerProps = {
      fluid : false,
      width:  window.innerWidth >  800 ? 900 : window.innerWidth,
      preload : "auto",
      src : this.props.url,
      poster : this.props.track.posterImage
    }

    const iframeStyle ={
        height : "720px",
        marginTop: "50px",
        border:'0px'
    }

    const showSpinner = () =>{
        if (this.state && this.state.player && isNaN(this.state.player.duration) && document.querySelector(".video-react-big-play-button") && document.querySelector(".video-react-big-play-button").style ){
            document.querySelector(".video-react-big-play-button").style.visibility ="hidden";
        }else if (this.state && this.state.player && parseInt(this.state.player.duration) > 0 && document.querySelector(".video-react-big-play-button") && document.querySelector(".video-react-big-play-button").style ){
            console.log("handled ..")
            document.querySelector(".video-react-big-play-button").style.visibility ="visible";
        }
        return true;
    }

    const wrapper = {
         margin: '0',
         position: 'absolute',
         top: '50%',
         left: '50%',
         transform: 'translate(-50%, -50%)',
         zIndex : '5'
        }

    const errorWrapper = {
        background: 'yellow',
        fontsize: '50px',
         margin: '0',
         position: 'absolute',
         top: '50%',
         left: '50%',
         transform: 'translate(-50%, -50%)',
         zIndex : '5'
        }

    return (

    <div >
                { window.innerWidth > 800 && 
        <div id="backoverlay" style={backoverlay} >
            <Back style={backoverlay.back}   onClick={goBack} ></Back>
        </div> }
        
    <div id="video-wrapper" style={wrapper}> 

        {
            this.state && this.state.player && isNaN(this.state.player.duration) && !this.state.player.error &&
            showSpinner() && 
            <div style={wrapper}>
            <svg className="spinner" width="65px" height="65px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
               <circle className="path" fill="none" strokeWidth="6" strokeLinecap="round" cx="33" cy="33" r="30"></circle>
            </svg>
        </div>
        }

        {
            this.state && this.state.player && this.state.player.error &&
            !this.props.url.indexOf("drive.google.com") > 0 &&
            <div style={errorWrapper}> Error loading video. </div>
        }


        {this.props.url  && this.props.url.indexOf("youtube.com") < 0 && 
            this.props.url.indexOf("drive.google.com") < 0 &&
        <div >
              <Player ref="player" 
                      fluid={playerProps.fluid} 
                      width={playerProps.width} 
                      preload={playerProps.preload} 
                      aspectRatio={playerProps.aspectRatio}
                      poster = {playerProps.poster}
                      src={playerProps.src}
                      autoPlay={true}
                      >
                  <ControlBar autoHide={true} />
                  <BigPlayButton position="center" />
              </Player>
        </div>
        }

        { 
           this.props.url  && 
            (this.props.url.indexOf("youtube.com") > 0 || this.props.url.indexOf("drive.google.com") > 0 ) && 
            <div className="infinite-list-container">
                <iframe src={this.props.url} width="1100px" height="650px" style={iframeStyle} allowFullScreen="allowfullscreen" />
            </div>
        }

    </div>
</div>
    );
  }
}