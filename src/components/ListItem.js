import React, { Component } from 'react';
import PropTypes from 'prop-types';
class ListItem extends Component {

  static propTypes = {
        item: PropTypes.number
  }

  render() {
    return (
        <div className="infinite-list-item">
        List Item 
        {this.props.item}
        </div>
    );
  }
}

export default ListItem;