import React, { Component } from 'react';
import $ from "jquery";
import MovieCard from './MovieCard';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import GoTop from 'material-ui/svg-icons/navigation/expand-less';
import CircularProgress from 'material-ui/CircularProgress';
import PropTypes from 'prop-types';

class InfiniteList extends Component {

    static propTypes = {
        data: PropTypes.array,
        onScroll: PropTypes.func,
        setSelectedTrack: PropTypes.func,
        isLast: PropTypes.bool,
        pageNo: PropTypes.number,
        isFetching: PropTypes.bool
    }

    onItemClick = (track) => {
        this.props.setSelectedTrack(track);
    };

    GoToTop = (e) => {
        e.preventDefault();
        window.scroll(0, 0);
    }

    componentWillReceiveProps(prop) {
        this.loadMore();
    }

    componentWillUnmount() {
        $(window).unbind('scroll');
    }

    loadMore() {
        $(window).unbind('scroll');
        $(window).bind('scroll', function () {
            if (Math.round($(window).scrollTop()) === Math.round($(document).height() - $(window).height())) {
                if (!this.props.isLast)
                    this.props.onScroll();
            }
        }.bind(this));
    }


    render() {

        //document.body.style.backgroundColor = "#E5E5E5";

        const styles = {
            floatingBarStyle: {
                margin: '0',
                top: 'auto',
                right: '20px',
                bottom: '20px',
                left: 'auto',
                position: 'fixed'
            },
            progress: {
                margin: '0 auto'
            },
            top: {
                marginTop: '50px'
            }
        }
        const loader = <div className="circular-progress"><CircularProgress style={styles.progress} /></div>;

        var tracks = this.props.data &&
            this.props.data.map((response, index) => {
                return (
                    <MovieCard track={response} onTouchTap={this.onItemClick} key={index}></MovieCard>
                )
            });

        return (
            <div className="infinite-list-container">
                {tracks}
                {this.props.isFetching && loader}
                {<FloatingActionButton style={styles.floatingBarStyle} onTouchTap={this.GoToTop}><GoTop /></FloatingActionButton>}
            </div>

        )

    }

}


export default InfiniteList;