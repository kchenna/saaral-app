import React, { Component } from 'react';
import './WallPaper.css';
import './Logo.css';
import PropTypes from 'prop-types';

export default class WallPaper extends Component {

    static propTypes = {
      backgroundImageUrl: PropTypes.string,
      logo:PropTypes.string,
      desc:PropTypes.string,
      heading:PropTypes.string,
      watchNow:PropTypes.func
    }


    render() {
        var img ='';
        if(this.props.backgroundImageUrl) {
            img = this.props.backgroundImageUrl;
            var tmp1 = img.substring(0,img.lastIndexOf("-"));
            var tmp2 = img.substring(img.lastIndexOf("."));
            img = tmp1 + tmp2;
        }

        return (
            <div id="hero" className="WallPaper" style={{backgroundImage: 'url('+img+')'}}>
                <div className="header"/>
                <div className="content">
                <img className="logo" src={this.props.logo} alt=""/>
                <h2 className="bold">{this.props.heading}</h2>
                <p>{this.props.desc}</p>
                <div className="button-wrapper">
                    { this.props.backgroundImageUrl && 
                        <div>
                            <a className="Button" 
                            data-primary={true} onClick={this.props.watchNow}>Watch now</a>
                        </div>
                    }
                </div>
                </div>
                <div className="overlay"></div>
            </div>
        );
    }
  
}
