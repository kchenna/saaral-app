import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import InfiniteList from '../components/InfiniteList';
import BackButton from '../components/BackButton';
import { actions as Action } from './Action';
import * as trackActions from '../action/TrackAction';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';

class Container extends Component {

    static propTypes = {
        actions: PropTypes.object,
        pageno : PropTypes.number,
        data : PropTypes.array,
        isLast : PropTypes.bool,
        isFetching : PropTypes.bool
    }

    componentDidMount = () => this.props.actions.getTracks(this.props.pageno);

    onScroll = () => this.props.actions.getTracks(this.props.pageno);
    

    render() {

        return (
            <div className="App-container">
                <BackButton back={this.props.router}/> 
                <InfiniteList 
                    data={this.props.data}
                    isLast={this.props.isLast}
                    isFetching={this.props.isFetching}
                    pageNo={this.props.pageno}
                    onScroll={this.onScroll}
                    setSelectedTrack={this.props.trackActions.setCurrentTrack}>
                </InfiniteList>
            </div>
         );
    }


}

const mapStateToProps = (state) => {
    return {
        data: state.liveTvReducer.data,
        pageno : state.liveTvReducer.pageno,
        isLast : state.liveTvReducer.isLast,
        isFetching: state.liveTvReducer.isFetching
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(Action, dispatch),
        trackActions:bindActionCreators(trackActions, dispatch)
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Container));