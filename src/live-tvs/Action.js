import { types } from './ActionType';
import * as MovieService from '../service/MovieService';

export const actions = {

    getTracks:(page) =>{
        return (dispatch) => {
            dispatch({ type: types.SET_LIVETV_FETCHING, isFetching: true });
            MovieService.getTracksByCategory(page, 10 , "LIVETV").then(function (response) {
                dispatch({ type: types.SET_LIVETV_FETCHING, isFetching: false });
                dispatch({ type: types.SET_LIVETV_LIST, pageno:page , data: response });
            });
        };
    }
    
}
