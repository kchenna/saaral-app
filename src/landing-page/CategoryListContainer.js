import React, { Component } from 'react';
import { connect } from 'react-redux';
import { actions as Action } from './Action';
import * as trackActions from '../action/TrackAction';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import CategoryContainer from './CategoryContainer';
import SearchContainer from './SearchContainer';

class CategoryListContainer extends Component {

    static propTypes = {
        actions: PropTypes.object,
        data: PropTypes.object,
        searchCriteria: PropTypes.string
    }

    componentDidMount() {
        this.props.categories.length ===0 && 
            this.props.actions.getCategories();
    }

    getCategoryTemplate=()=>{
        var categories = this.props.categories &&
            this.props.categories.map((response, index) => {
                return (
                    <CategoryContainer category={response.name} key={index}></CategoryContainer>
                )
            });

         return(
             <div>
                { categories }
            </div>
        );
    }

    getSearchTemplate=()=>{
        return (
            <div>
                <SearchContainer/>
            </div>
        );
    }

    render() {
        return (
            this.props.searchCriteria.length > 0 ? 
                this.getSearchTemplate() :
            this.getCategoryTemplate()
        );
    }
}

const mapStateToProps = (state) => {
    return {
        categories : state.home.categories,
        search : state.home.search,
        searchCriteria: state.home.searchCriteria
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(Action, dispatch),
        trackActions:bindActionCreators(trackActions, dispatch)
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CategoryListContainer);
