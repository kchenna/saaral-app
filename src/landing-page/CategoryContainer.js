import React, { Component } from 'react';
import { connect } from 'react-redux';
import { actions as Action } from './Action';
import * as trackActions from '../action/TrackAction';
import { bindActionCreators } from 'redux';
import SliderWidget from '../components/SliderWidget';
import PropTypes from 'prop-types';
import { find } from 'lodash';

class CategoryContainer extends Component {

    static propTypes = {
        actions: PropTypes.object,
        category: PropTypes.string,
    }

    componentDidMount() {
        let category = find(this.props.categoriesData, { name: this.props.category });
        category.data.length === 0 && this.props.actions.getCategoryTrack(
            this.props.category
        );  
    }

    render() {
        let category = find(this.props.categoriesData, { name: this.props.category });
        return (
            category.active && category.data.length > 0 ? 
                <SliderWidget
                    title={category.title}
                    data={category.data}
                    onTouchTap={this.props.trackActions.setCurrentTrack}
                    onViewAll={this.props.trackActions.routeTo}
                    path={category.name} /> :
            null
        );
    }

}

const mapStateToProps = (state) => {
    return {
        categoriesData: state.home.categories
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(Action, dispatch),
        trackActions:bindActionCreators(trackActions, dispatch)
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CategoryContainer);
