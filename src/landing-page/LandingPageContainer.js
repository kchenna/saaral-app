import React, { Component } from 'react';
import WallPaperContainer from "./WallPaperContainer";
import CategoryListContainer from "./CategoryListContainer";
import { withRouter } from 'react-router';
import { connect } from 'react-redux';

class LandingPageContainer extends Component {

  render() {
    return (
      <div>
        <WallPaperContainer/>
        <CategoryListContainer/>
      </div>
    );
  }

};

export default connect(null, null)(withRouter(LandingPageContainer));

