import React, { Component } from 'react';
import { connect } from 'react-redux';
import { actions as Action } from './Action';
import { bindActionCreators } from 'redux';
import WallPaper from '../components/WallPaper';
import PropTypes from 'prop-types';
import SearchBar from 'material-ui-search-bar'

class WallPaperContainer extends Component {

    constructor(props){
        super(props);
        this.state = {
            criteria:''
        }
    }
    static propTypes = {
        actions: PropTypes.object,
        wallPaper: PropTypes.object,
        showWallpaper : PropTypes.bool
    }

    componentDidMount() {
        !this.props.wallPaper.category && this.props.actions.getLatestTrack();
    }

    watchNow = () => {
        this.props.actions.setCurrentTrack(this.props.wallPaper);
    }

    onSearch =(criteria) =>{
        if(criteria !== undefined){
            this.setState({criteria:criteria});
        }
        
        if ( criteria && criteria.length > 0){
            this.props.actions.searchCategoriesTrack(criteria);
        }else{
            this.props.actions.clearSearch();
        }

    }

    render() {
        return (
            <div>
                {
                    this.props.wallPaper !==null &&
                    <div>
                        <SearchBar
                            onChange={(value) => this.onSearch(value) }
                            onRequestSearch={(value) => this.onSearch(value) }
                            style={{
                                maxWidth: 500,
                                position:'absolute',
                                right:'50px',
                                top:'10px',
                                zIndex:100
                            }}
                        />

                        {
                        (!this.state.criteria || this.state.criteria.length < 1) &&
                        <WallPaper
                            watchNow={this.watchNow}
                            backgroundImageUrl={this.props.wallPaper.posterImage}
                            desc="Enjoy the latest episode with your family and have fun"
                            heading="Season 1 now available"/>
                        }
                    </div>
                    }
            </div>
        );
    }

}

const mapStateToProps = (state) => {
    return {
        wallPaper: state.home.wall_paper
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(Action, dispatch)
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(WallPaperContainer);
