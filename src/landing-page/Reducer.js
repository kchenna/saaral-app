import types from './ActionType';
import { getTrackFromLocalStorage, setTrackFromLocalStorage } from '../storage/LocalStorage';


export const initialState = () => {
    return {
        current: getTrackFromLocalStorage(),
        wall_paper: {},
        categories:[],
        search:[],
        searchCriteria:''
    };
};

const setCategories = (categories) =>{
    let r =[];
    categories && categories.forEach(function(category){
        let c = { 
            name : category.name,
            title : category.description,
            data :[],
            error:null,
            fetching:false,
            active : category.active};
        r.push(c);
    });
    return r;
}

const updateFetchingInCategory = (array, action) => {
    return array.map( (item, index) => {
        if(item.name !== action.category) {
            return item;
        }
        return {
            ...item,
            ...action.fetching
        };  
    });
}


const updateErrorInCategory = (array, action) => {
    return array.map( (item, index) => {
        if(item.name !== action.category) {
            return item;
        }
        return {
            ...item,
            ...action.error
        };  
    });
}

const updateDataInCategory = (array, action) => {
    return array.map( (item, index) => {
        if(item.name !== action.category) {
            return item;
        }
        return {
            ...item,
            data:action.data
        };    
    });
}

export default (state = initialState(), action) => {

    switch (action.type) {
        case types.SET_SEARCH_CRITERIA:
        return {
            ...state,
            searchCriteria:action.searchCriteria
        }
        case types.SET_SEARCH_CATEGORIES:
        return {
                ...state,
                search:action.data
            };
        case types.RESET_SEARCH_DATA:
        return {
            ...state,
            search:[],
            searchCriteria:''
        }
        case types.SET_LANDING_CATEGORY_FETCHING:
            return {
                ...state,
                categories : updateFetchingInCategory(state.categories,action)
            }
        case types.SET_LANDING_CATEGORY_DATA:
            return {
                ...state,
                categories : updateDataInCategory(state.categories,action)
            };
        case types.SET_LANDING_CATEGORY_ERROR:
            return {
                ...state,
                categories : updateErrorInCategory(state.categories,action)
            };
        case types.SET_CATEGORIES:
            return {
                ...state,
                categories:setCategories(action.categories)
            };
        case types.SET_CURRENT_TRACK:
            setTrackFromLocalStorage(action.track);
            return {
                ...state,
                current: action.track
            };
        case types.LANDING_WALLPAPER_DATA:
            return {
                ...state,
                wall_paper: action.landing
            };
        default:
            return state;
    }
};
