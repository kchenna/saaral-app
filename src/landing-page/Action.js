import types from './ActionType';
import * as MovieService from '../service/MovieService';

export const actions = {
    
    getCategories :() =>{
        return (dispatch) => {
            MovieService.getCategories().then(function(response) {
                dispatch({ type: types.SET_CATEGORIES , categories : response });
            });
          };
    },

    clearSearch:()=>{
        return ({ type: types.RESET_SEARCH_DATA});
    },

    searchCategoriesTrack :(criteria) =>{
        return (dispatch) => {
            dispatch({ type: types.SET_SEARCH_CRITERIA, searchCriteria:criteria });
            MovieService.searchCategories(criteria).then(function(response) {
                dispatch({ type: types.SET_SEARCH_CATEGORIES , data: response });
            });
          };
    },

    getCategoryTrack :(cat) =>{
        return (dispatch) => {
            dispatch({ type: types.SET_LANDING_CATEGORY_FETCHING,category:cat, fetching: true });
            MovieService.getTracksByCategory(1, 20 ,cat)
            .then((response)=> {
                dispatch({ type: types.SET_LANDING_CATEGORY_DATA, category:cat , data: response.content });
                dispatch({ type: types.SET_LANDING_CATEGORY_FETCHING,category:cat, fetching: false });
            })
            .catch((e)=>{
                dispatch({ type: types.SET_LANDING_CATEGORY_ERROR,category:cat, error: e });
                dispatch({ type: types.SET_LANDING_CATEGORY_FETCHING,category:cat, fetching: false });
            });
          };
    },

    getLatestTrack : () =>{
         return (dispatch) => {
            MovieService.getTracksByCategory(1,1,"TVSHOWS").then(function(response) {
                dispatch({ type: types.LANDING_WALLPAPER_DATA , landing : response.content[0] });
            });
          };
    }
}