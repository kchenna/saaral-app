import React, { Component } from 'react';
import { connect } from 'react-redux';
import { actions as Action } from './Action';
import * as trackActions from '../action/TrackAction';
import { bindActionCreators } from 'redux';
import SliderWidget from '../components/SliderWidget';
import PropTypes from 'prop-types';

class SearchContainer extends Component {

    static propTypes = {
        actions: PropTypes.object,
        category: PropTypes.string,
        searchCriteria: PropTypes.string
    }

    render() {
        let title = '('+this.props.search.totalElements + ') results for search';
        return (
                <SliderWidget
                    title={title}
                    data={this.props.search.content}
                    onTouchTap={this.props.trackActions.setCurrentTrack}
                    onViewAll={this.props.trackActions.routeTo} />
        );
    }

}

const mapStateToProps = (state) => {
    return {
        search: state.home.search,
        searchCriteria: state.home.searchCriteria
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(Action, dispatch),
        trackActions:bindActionCreators(trackActions, dispatch)
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchContainer);
