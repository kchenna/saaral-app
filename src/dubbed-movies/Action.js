import { types } from './ActionType';
import * as MovieService from '../service/MovieService';

export const actions = {

    getDubbedMovieTracks:(page) =>{
        return (dispatch) => {
            dispatch({ type: types.SET_DUBBED_MOVIE_FETCHING, isFetching: true });
            MovieService.getTracksByCategory(page, 21 , "DUBBED").then(function (response) {
                dispatch({ type: types.SET_DUBBED_MOVIE_FETCHING, isFetching: false });
                dispatch({ type: types.SET_DUBBED_MOVIE_LIST, pageno:page , data: response });
            });
        };
    }
    
}
