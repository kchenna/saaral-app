export const types = {
    SET_TVSHOW_LIST: 'SET_MOVIE_LIST',
    SET_TVSHOW_FETCHING:'SET_FETCHING',
    SET_TVSHOW_ERROR:'SET_ERROR',
    SET_CURRENT_TRACK:'SET_CURRENT_TRACK'
}
