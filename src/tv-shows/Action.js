import { types } from './ActionType';
import * as MovieService from '../service/MovieService';

export const actions = {

    getTVShowTracks:(page) =>{
        return (dispatch) => {
            dispatch({ type: types.SET_TVSHOW_FETCHING, isFetching: true });
            MovieService.getTracksByCategory(page, 21 , "TVSHOWS").then(function (response) {
                dispatch({ type: types.SET_TVSHOW_FETCHING, isFetching: false });
                dispatch({ type: types.SET_TVSHOW_LIST, pageno:page , data: response });
            });
        };
    }
    
}
