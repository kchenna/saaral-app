import { types } from './ActionType';

export const initialState = () => {
    return {
        data: [],
        isLastPage :false,
        isFetching : false,
        pageno : 1,
        error:{}
    };
};

export default (state = initialState(), action) => {
    switch (action.type) {
         case types.SET_TAMIL_MOVIE_LIST:
            return {
                ...state,
                data: state.data.concat(action.data.content),
                isLastPage : action.data.last,
                pageno : action.pageno + 1
            };  
        case types.SET_TAMIL_MOVIE_FETCHING:
            return {
                ...state,
                isFetching : action.isFetching
            };
        case types.SET_TAMIL_MOVIE_ERROR:
            return {
                ...state,
                error : action.error
            };  
        default:
            return state;
    }
};
