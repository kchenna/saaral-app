import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import InfiniteList from '../components/InfiniteList';
import { actions as Action } from './Action';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import BackButton from '../components/BackButton';
import * as trackActions from '../action/TrackAction';

class Container extends Component {

    static propTypes = {
        actions: PropTypes.object,
        pageno : PropTypes.number,
        data : PropTypes.array,
        isLast : PropTypes.bool,
        isFetching : PropTypes.bool
    }

    componentDidMount = () => this.props.actions.getMovieTracks(this.props.pageno);

    onScroll = () => this.props.actions.getMovieTracks(this.props.pageno);
    
    render() {
        return (
            <div className="App-container">
                <BackButton back={this.props.router}/> 
                <InfiniteList 
                    data={this.props.data}
                    isLast={this.props.isLast}
                    isFetching={this.props.isFetching}
                    pageNo={this.props.pageno}
                    onScroll={this.onScroll}
                    setSelectedTrack={this.props.trackActions.setCurrentTrack}>
                </InfiniteList>
            </div>
         );
    }


}

const mapStateToProps = (state) => {
    return {
        data: state.tamilMovieReducer.data,
        pageno : state.tamilMovieReducer.pageno,
        isLast : state.tamilMovieReducer.isLast,
        isFetching: state.tamilMovieReducer.isFetching
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(Action, dispatch),
        trackActions: bindActionCreators(trackActions, dispatch)
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Container));