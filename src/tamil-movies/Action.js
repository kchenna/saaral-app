import { types } from './ActionType';
import * as MovieService from '../service/MovieService';

export const actions = {

    getMovieTracks:(page) =>{
        return (dispatch) => {
            dispatch({ type: types.SET_TAMIL_MOVIE_FETCHING, isFetching: true });
            MovieService.getTracksByCategory(page, 21 , "MOVIES").then(function (response) {
                dispatch({ type: types.SET_TAMIL_MOVIE_FETCHING, isFetching: false });
                dispatch({ type: types.SET_TAMIL_MOVIE_LIST, pageno:page , data: response });
            });
        };
    }
    
}
