import React from 'react';
import { Provider } from 'react-redux';
import { hashHistory, Route, Router, IndexRoute } from 'react-router';
import AppContainer from './App';
import PlayerContainer from './player/Container';
import TVShowsContainer from './tv-shows/Container';
import TamilMoviesContainer from "./tamil-movies/Container";
import DubbedMoviesContainer from "./dubbed-movies/Container";
import LiveTvContainer from "./live-tvs/Container";

import store from "./store";

import LandingPageContainer from './landing-page/LandingPageContainer';

export const routes = (
    <Route
        component={AppContainer}
        path="/"
    >
        <IndexRoute component={LandingPageContainer}/>
        <Route
            component={LandingPageContainer}
            key="landing"
            path="landing"
        />
        <Route
            component={PlayerContainer}
            key="player"
            path="player"
        />
        <Route
            component={TVShowsContainer}
            key="TVSHOWS"
            path="TVSHOWS"
        />
        <Route
            component={TamilMoviesContainer}
            key="MOVIES"
            path="MOVIES"
        />
        <Route
            component={DubbedMoviesContainer}
            key="DUBBED"
            path="DUBBED"
        />
        <Route
            component={LiveTvContainer}
            key="livetv"
            path="livetv"
        />
    </Route>
);

export default (
    <Provider store={store()}>
        <Router
            history={hashHistory}
            routes={routes}
        />
    </Provider>
);
