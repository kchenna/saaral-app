import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { routerMiddleware, routerReducer } from 'react-router-redux';
import { hashHistory } from 'react-router';
import home from './landing-page/Reducer';
import tamilMovieReducer from './tamil-movies/Reducer';
import dubbedMovieReducer from './dubbed-movies/Reducer';
import tvShowReducer from './tv-shows/Reducer';
import liveTvReducer from './live-tvs/Reducer';
import player from './player/Reducer';

const rootReducer = combineReducers({
    routerReducer,
    home,
    tamilMovieReducer,
    dubbedMovieReducer,
    liveTvReducer,
    tvShowReducer,
    player
});

const middleWare = [thunkMiddleware, routerMiddleware(hashHistory)];
const initStore = (initialState) => {
    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose; /*eslint no-underscore-dangle: ["error", { "allow": ["__REDUX_DEVTOOLS_EXTENSION_COMPOSE__"] }]*/

    return createStore(
        rootReducer,
        initialState,
        composeEnhancers(
            applyMiddleware(...middleWare)
        )
    );
};

export default initStore;