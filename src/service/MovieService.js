//import {mock} from './../mock/MockApi'; // NOTE : If mock is preset it will call the mock and return the result

const api = {
    domain : 'https://spsenthil.com:8443/'
}

export const getPlayLinkUrl = (playUrl,referredUrl) =>{

    let url = api.domain +'movie/tamil/source';
    var request = new Request(url,
        {
            method: 'POST', 
            mode: 'cors',
            headers: new Headers({'content-type': 'application/json'}),
            body: JSON.stringify({movieLink:playUrl,referer:referredUrl})
        }
    );

    return window.fetch(request).then(function(resp) { 
        return (resp.json());
    });

};

export const getTracks = (page,size) =>{

    let url = api.domain +'movie/tamil/movies/'+page + '?size=' + size;
    return window.fetch(url).then(function(resp) { 
        return resp.json();
    });
}

export const getTracksByCategory = (page,size,category) =>{

    let url = api.domain +'movie/tamil/movies/'+page + '?size=' + size + '&category=' + category;
    return window.fetch(url).then(function(resp) { 
        return resp.json();
    });

}

export const searchCategories = (searchCriteria) =>{

    let url = api.domain +'/movie/tamil/search/'+searchCriteria;
    return window.fetch(url).then(function(resp) { 
        return resp.json();
    });

}


export const getCategories = () =>{

    let url = api.domain +'movie/tamil/categories/all';
    return window.fetch(url).then(function(resp) { 
        return resp.json();
    });

}
