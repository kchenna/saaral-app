import './App.css';
import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';

class App extends Component {

  render() {
    return (
          <div>
              {this.props.children}
        </div>
    );
  }
}

export default connect(null, null)(withRouter(App));

