export const LOCAL_STORAGE_CURRENT_TRACK = "LOCAL_STORAGE_CURRENT_TRACK";

export const getTrackFromLocalStorage = () => {
    let track = window.localStorage.getItem(LOCAL_STORAGE_CURRENT_TRACK);
    return !track ? JSON.parse(track) : null;
};

export const setTrackFromLocalStorage = (track) => {
    window.localStorage.setItem(LOCAL_STORAGE_CURRENT_TRACK, JSON.stringify(track));
};

