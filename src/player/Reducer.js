import { types } from './ActionType';

export const initialState = () => {
    return {
        url:undefined,
        fetching:false
    };
};

export default (state = initialState(), action) => {
    switch (action.type) {
        case types.SET_PLAYER_URL :
            return {
                ...state,
                url : action.url
            }
        case types.SET_PLAYER_URL_FETCHING:
            return {
                ...state,
                fetching:action.fetching
            }
        default:
            return state;    
    }
};
