import { types } from './ActionType';
import { routerActions } from 'react-router-redux';
import * as MovieService from '../service/MovieService';
import _ from 'lodash';
export const actions = {

    getPlayer: (track) => {
        return (dispatch) => {
            var metadata = _.find(track.movieLinks, function (meta) {
                return meta.default ===/**/ true
            });

            if (metadata.file.indexOf(".mp4") !== -1) {
                dispatch({ type: types.SET_PLAYER_URL, url: metadata.file });
            } else {
                dispatch({ type: types.SET_PLAYER_URL_FETCHING, fetching: true });

                MovieService.getPlayLinkUrl(metadata.file, track.baseUrl)
                    .then(function (resp) {
                        if (resp) {
                            dispatch({ type: types.SET_PLAYER_URL, url: resp.movieLink });
                            dispatch({ type: types.SET_PLAYER_URL_FETCHING, fetching: false });
                        }
                    }
                    );
            }
        };
    },

    setPlayer: (track) => {
        return (dispatch) => {
            dispatch({ type: types.SET_CURRENT_TRACK, track });
            dispatch(routerActions.push('/player'));
        };
    }
}
