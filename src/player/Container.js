import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import MoviePlayer from './MoviePlayer';

import { bindActionCreators } from 'redux';
import { actions as Action } from './Action';
import PropTypes from 'prop-types';

export class Container extends Component {

    static propTypes = {
        track: PropTypes.object,
        player: PropTypes.object,
        actions: PropTypes.object,
        isFetching: PropTypes.isFetching
    };


    componentDidMount() {
        console.log("props.track ", this.props.track);
        this.props.actions.getPlayer(this.props.track);
    }

    render() {
        let currentTrack = this.props.track;
        return (
            <div>
                <MoviePlayer
                    track={currentTrack}
                    url={this.props.url}
                    isFetching={this.props.isFetching}
                    router={this.props.router} />
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        track: state.home.current,
        url: state.player.url,
        isFetching: state.player.fetching
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(Action, dispatch)
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Container));