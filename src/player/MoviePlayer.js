import React, { Component } from 'react';
import CustomPlayer from './CustomPlayer';
import PropTypes from 'prop-types';

export default class MoviePlayer extends Component {

    static propTypes = {
        url: PropTypes.string,
        isFetching: PropTypes.bool,
        track: PropTypes.object
    };



    render() {


        const wrapper = {
            margin: '0',
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            zIndex: '5'
        }

        const errorWrapper = {
            background: 'yellow',
            fontsize: '50px',
            margin: '0',
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            zIndex: '5'
        }

        document.body.style.backgroundColor = "black";
        return (
            <div id='movie_player'>

                {
                    this.props.isFetching &&
                    <div style={wrapper}>
                        <svg className="spinner" width="65px" height="65px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
                            <circle className="path" fill="none" strokeWidth="6" strokeLinecap="round" cx="33" cy="33" r="30"></circle>
                        </svg>
                    </div>
                }

                {
                    !this.props.isFetching && this.props.url !== null &&
                    <CustomPlayer router={this.props.router} url={this.props.url}
                        track={this.props.track}></CustomPlayer>
                }

                {
                    !this.props.track.movieLinks &&
                    <div style={errorWrapper}> Error loading video. </div>
                }

            </div>
        );
    }
}